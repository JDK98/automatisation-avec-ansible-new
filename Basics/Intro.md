Intro
---

`ansible all -i hosts -m ping` <br>

Les fichiers de configurations ansible : `/etc/ansible/ansible.cfg`  `~/.ansible.cfg`

**~/.ansible.cfg** OU **ansible.cfg** dans le projet
```
[defaults]
host_key_checking = False
```

Pour les versions de paquet `launchpad.net`

Ansible-galaxy contient les collection ansible (Disponible pour la dernière version ansible uniquement)
`ansible-galaxy collection list`