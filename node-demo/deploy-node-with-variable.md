```yaml
---
- name: Deploy nodejs app
  hosts: aws
  # CECI EST UNE MANIERE DE DECLARER LES VARIABLES
  #vars:
  #  - app_name: nodejs-app
  #  - version: 1.0.0
  #  - destination: /home/ubuntu
  vars_files:
    - variables.yaml
  tasks: 
    - name: Copy and Unpacking the archive
      unarchive:
        src: "{{app_name}}-{{version}}.tgz"
        dest: "{{destination}}"
    - name: Install the dependencies
      community.general.npm:
        path: /home/ubuntu/package
    - name: Start the server
      command:
        chdir: /home/ubuntu/package/app
        cmd: node server
      async: 1000
      poll: 0
    - name: Ensure App is running
      shell: ps aux | grep node
      register: app_status
    - debug: msg={{app_status.stdout_lines}}
```

**Déclarer les variables par Command Line** <br>
`ansible-playbook -i hosts deploy-node.yaml -e "app_name=nodejs-app version=1.0.0 destination=/home/ubuntu"`


